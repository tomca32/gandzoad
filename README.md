#Gandzo Ads

Gandzo krece u oglasavanje.

Dakle sistem je relativno jednostavan. Trebas prvo negdje ubaciti ad.js ili ad.min.js. To napravis jednostavnim script tagom:

    <script src="path/to/ad.min.js"></script>

Nakon toga gdje god zelis oglas trebas pozvati GandzoAd i proslijedit mu settingse:

    <script>new GandzoAd({ovdjePisesSvojeSettings})</script>

Ispod mozes vidjeti primjer sa settingsima a imas primjer i u index.html fileu.

##Settings:

- width : Number - Sirina oglasa u pixelima, default 300
- bodyBackground : String/CSS color value - boja pozadine, default white
- bodyColor: String/CSS color value - boja texta oglasa, default black
- headerBackground: : String/CSS color value - boja pozadine headera
- headerColor: String/CSS color value - boja texta headera, default white
- headerContent : String - content headera
- headerPadding : Number - padding headera u pixelima, default 5
- headerFontSize: Number - velicina fonta headera u px, default 20
- headingColor: String/CSS color value - boja headinga pojedinog oglasa, default blue
- headingFontSize: Number - velicina heading pojedinog oglasa, default 14
- bodyPadding: Number - padding body oglasa, default 10
- bodyFontSoze: Number - velicina texta oglasa, default 12
- imageWidth: Number (0-1) - postotak sirine slike u odnosu na kompletan width, default 0.3 (30%)
- borderColor: String/CSS color value - boja bordera, default black
- borderWidth: Number - sirina bordera, default 1
- content: Array of Objects - content pojedinog oglasa, detaljnije ispod
- footerContent: String - content footera
- footerFontSize: Number - velicina fonta footera

##Content

Content se prosljedjuje kao Array objekata. Svaki objekt se sastoji od imagea, headinga, contenta i linka:

- image: String - URL do slike oglasa
- heading: String - Heading oglasa
- content: String - content pojedinog oglasa
- link: String - link oglasa, bit ce povezan sa slikom i headingom

##Example

    new GandzoAd({
      width: 350,
      headerColor: 'red',
      headerBackground: 'white',
      headerContent: 'This is an example Ad',
      content: [{
        image: 'http://placehold.it/100x50',
        heading: 'THIS IS AN AD',
        content: 'It sure is an ad. Look at it all shiny and new',
        link: 'http:google.com'
      }]
    });