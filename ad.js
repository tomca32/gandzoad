;(function(exports){
  'use strict';

//this is the HTML template for the add. We use $propertyName as placeholders for settings

var defaultHTML = '<div class="adContainer" style="border: solid $borderColor $borderWidthpx; width: $widthpx">'; //opening container div

defaultHTML += '<div class="adHeader" style="background: $headerBackground; padding: $headerPaddingpx; color: $headerColor; text-align: center; font-size: $headerFontSizepx;">$headerContent</div>'; //header div
defaultHTML += '<div class="adBody" style="padding: $bodyPaddingpx; background: $bodyBackground;">$adsHere';
defaultHTML += '<div class="adFooter" style="float:right; font-size:$footerFontSizepx">$footerContent</div>'; //footer div
defaultHTML += '<div style="clear:both;"></div>'; //closing adBody div
defaultHTML += '</div>'; //closing adBody div
defaultHTML += '</div>'; //closing container div


var defaultSettings = {
  width: 300,
  bodyBackground: 'white',
  bodyColor: 'black',
  headerBackground: 'blue',
  headerColor:'white',
  headerContent: 'I AM A HEADER',
  headerPadding: 5,
  headerFontSize: 20,
  headingColor: 'blue',
  headingFontSize: 14,
  bodyPadding: 10,
  bodyFontSize: 12,
  imageWidth: 0.3,
  borderColor: 'black',
  borderWidth: 1,
  content: [{
    image: 'http://placehold.it/350x150',
    heading: 'I AD',
    content: 'Ad text goes here',
    link: 'http://google.com'
  }],
  footerContent: 'Gandzo Ad, MEOW!',
  footerFontSize: 12
};

function parseSettings(settings) {
  var ret = {}, p;
  if (typeof settings === 'undefined') {settings = {};}
  for (p in defaultSettings) {
    if (defaultSettings.hasOwnProperty(p)) {
      ret[p] = settings[p] || defaultSettings[p];
    }
  }
  return ret;
}

function writeAd(s) {
  //this replaces document.write because document.write is evil!
  var scripts = document.getElementsByTagName('script');
  var lastScript = scripts[scripts.length-1];
  lastScript.insertAdjacentHTML('beforebegin', s);
}

function GandzoAd(settings) {
  //Ad constructor
  this.settings = parseSettings(settings);
  
  writeAd(this.parseHTML(this.insertContent(defaultHTML)));  
}

GandzoAd.prototype.insertContent = function(HTML) {
  var i, ret = '<table style="margin-bottom:10px;">', c = this.settings.content;

  for (i = 0; i < c.length; i+=1) {
    ret += '<tr><td style="width: '+ this.settings.imageWidth * (this.settings.width - this.settings.bodyPadding) +'px;">' + (c[i].image ? '<a href="' + c[i].link + '" target="_blank"><img src="' + c[i].image + '" style="width: 100%;"></a></td>' : '</td>');
    ret += '<td style="vertical-align: top;"><div class="adHeading" style="color: $headingColor; font-sizepx: $headingFontSize;"><a href="' + c[i].link + '" style="color: $headingColor" target="_blank">' + c[i].heading +'</a></div>';
    ret += '<span class="adContent" style="color: $bodyColor; font-size:$bodyFontSizepx;">' + c[i].content + '</span></a>';

  }
  ret += '</table>';
  

  return HTML.replace(/\$adsHere/,ret);
};

GandzoAd.prototype.parseHTML = function(HTML) {
  //we replace $crap in our HTML template with the actual settings
  var ret = HTML, p, re;
  for (p in this.settings) {
    re = new RegExp('\\$'+p, 'g');
    ret = ret.replace(re, this.settings[p]);
  }
  return ret;
};




exports.GandzoAd = GandzoAd;




})(this);